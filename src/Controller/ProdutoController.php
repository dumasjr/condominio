<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProdutoController extends AbstractController
{
    /**
     * @Route("/produto", name="produto")
     */
    public function index()
    {

        $em = $this->getDoctrine()->getManager();

        $produto = $em->getRepository(className:Produto::class)->findAll();

        return $this->render("produto/index.html.twig",['produto'=> $produtos ]);


    }
}
